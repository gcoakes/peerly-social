/// <reference types="svelte" />
/// <reference types="vite/client" />
import type * as IPFS from "ipfs-core-types";

declare global {
  interface Window {
    IpfsCore: IPFS;
    ipfs: IPFS.IPFS;
  }
}
