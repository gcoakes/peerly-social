import { onDestroy, onMount } from "svelte";

export function mounted(): Promise<void> {
  return new Promise((resolve, reject) => {
    onDestroy(reject);
    onMount(resolve);
  });
}
