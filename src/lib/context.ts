import type { IPFS } from "ipfs-core-types";
import { getContext } from "svelte";

export const ipfsKey = {};
export function ipfs(): IPFS {
  return getContext(ipfsKey);
}
