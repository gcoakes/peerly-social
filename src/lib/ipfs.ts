import type { IPFS } from "ipfs-core-types";
import { CID } from "multiformats/cid";

let initialized = false;
const callbacks: {
  resolve: (IPFS) => void;
  reject: () => void;
}[] = [];

export default function ipfs(): Promise<IPFS> {
  if (initialized) return Promise.resolve(window.ipfs);
  return new Promise((resolve, reject) => {
    callbacks.push({ resolve, reject });
  });
}

export async function init(): Promise<void> {
  try {
    if (window.ipfs === undefined) {
      window.ipfs = await window.IpfsCore.create();
    }
    initialized = true;
    for (const { resolve } of callbacks) {
      resolve(window.ipfs);
    }
  } catch {
    for (const { reject } of callbacks) {
      reject();
    }
  }
}

export function parseCID(cid: string): CID | null {
  try {
    return CID.parse(cid);
  } catch {
    return null;
  }
}
