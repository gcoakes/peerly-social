import { onMount } from "svelte";
import type { Readable, Writable } from "svelte/store";
import { writable } from "svelte/store";

export function local<T>(key: string, def?: T): Writable<T> {
  const store = writable<T>(def);
  const listener = (ev) => {
    if (ev.key === key && ev.newValue != ev.oldValue) {
      store.set(JSON.parse(ev.newValue));
    }
  };
  onMount(() => {
    const value = window.localStorage.getItem(key);
    if (value) {
      store.set(JSON.parse(value));
    }
    store.subscribe((value) => {
      if (value === undefined || value === null) {
        window.localStorage.removeItem(key);
      } else {
        window.localStorage.setItem(key, JSON.stringify(value));
      }
    });
    window.addEventListener("storage", listener);
    return () => {
      window.addEventListener("storage", listener);
    };
  });
  return store;
}

export function dataAttribute<T extends string>(
  key: string,
  element?: Readable<Element>,
  def?: T
): Writable<T> {
  const store = local<T>(key, def);
  if (element === undefined) {
    element = writable(null);
    onMount(() => {
      // @ts-ignore: The element has been created as a writable internally.
      element.set(document.documentElement);
    });
  }
  element.subscribe((el) =>
    el
      ? store.subscribe((value) => {
          const attr = "data-" + key;
          if (value === undefined || value === null) {
            el.removeAttribute(attr);
          } else {
            el.setAttribute(attr, value);
          }
        })
      : null
  );
  return store;
}

export default local;
