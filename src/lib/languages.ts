import cpp_js from "highlight.js/es/languages/cpp.js";
import gradle from "highlight.js/es/languages/gradle";
import lisp_js from "highlight.js/es/languages/lisp.js";
import maxima from "highlight.js/es/languages/maxima";
import python_repl from "highlight.js/es/languages/python-repl";
import dts_js from "highlight.js/es/languages/dts.js";
import fix from "highlight.js/es/languages/fix";
import monkey_js from "highlight.js/es/languages/monkey.js";
import oxygene_js from "highlight.js/es/languages/oxygene.js";
import bash from "highlight.js/es/languages/bash";
import purebasic_js from "highlight.js/es/languages/purebasic.js";
import cos_js from "highlight.js/es/languages/cos.js";
import rust from "highlight.js/es/languages/rust";
import clean from "highlight.js/es/languages/clean";
import angelscript from "highlight.js/es/languages/angelscript";
import lua_js from "highlight.js/es/languages/lua.js";
import inform7 from "highlight.js/es/languages/inform7";
import livescript from "highlight.js/es/languages/livescript";
import autohotkey_js from "highlight.js/es/languages/autohotkey.js";
import axapta_js from "highlight.js/es/languages/axapta.js";
import puppet from "highlight.js/es/languages/puppet";
import swift_js from "highlight.js/es/languages/swift.js";
import pf from "highlight.js/es/languages/pf";
import smali from "highlight.js/es/languages/smali";
import step21_js from "highlight.js/es/languages/step21.js";
import tcl_js from "highlight.js/es/languages/tcl.js";
import julia_repl_js from "highlight.js/es/languages/julia-repl.js";
import php_template_js from "highlight.js/es/languages/php-template.js";
import python_js from "highlight.js/es/languages/python.js";
import coq from "highlight.js/es/languages/coq";
import dos from "highlight.js/es/languages/dos";
import vbscript_js from "highlight.js/es/languages/vbscript.js";
import ini_js from "highlight.js/es/languages/ini.js";
import perl from "highlight.js/es/languages/perl";
import stan from "highlight.js/es/languages/stan";
import irpf90 from "highlight.js/es/languages/irpf90";
import ini from "highlight.js/es/languages/ini";
import nestedtext from "highlight.js/es/languages/nestedtext";
import coffeescript_js from "highlight.js/es/languages/coffeescript.js";
import vim from "highlight.js/es/languages/vim";
import wren from "highlight.js/es/languages/wren";
import crmsh from "highlight.js/es/languages/crmsh";
import prolog from "highlight.js/es/languages/prolog";
import scss_js from "highlight.js/es/languages/scss.js";
import fortran_js from "highlight.js/es/languages/fortran.js";
import clojure_repl from "highlight.js/es/languages/clojure-repl";
import awk from "highlight.js/es/languages/awk";
import elm from "highlight.js/es/languages/elm";
import asciidoc from "highlight.js/es/languages/asciidoc";
import css from "highlight.js/es/languages/css";
import gherkin from "highlight.js/es/languages/gherkin";
import autoit from "highlight.js/es/languages/autoit";
import d from "highlight.js/es/languages/d";
import rsl from "highlight.js/es/languages/rsl";
import autohotkey from "highlight.js/es/languages/autohotkey";
import diff_js from "highlight.js/es/languages/diff.js";
import sas from "highlight.js/es/languages/sas";
import openscad from "highlight.js/es/languages/openscad";
import mel_js from "highlight.js/es/languages/mel.js";
import gcode from "highlight.js/es/languages/gcode";
import handlebars from "highlight.js/es/languages/handlebars";
import fsharp_js from "highlight.js/es/languages/fsharp.js";
import roboconf from "highlight.js/es/languages/roboconf";
import scss from "highlight.js/es/languages/scss";
import mathematica from "highlight.js/es/languages/mathematica";
import reasonml from "highlight.js/es/languages/reasonml";
import r from "highlight.js/es/languages/r";
import purebasic from "highlight.js/es/languages/purebasic";
import typescript_js from "highlight.js/es/languages/typescript.js";
import maxima_js from "highlight.js/es/languages/maxima.js";
import basic from "highlight.js/es/languages/basic";
import vhdl_js from "highlight.js/es/languages/vhdl.js";
import gml_js from "highlight.js/es/languages/gml.js";
import c_js from "highlight.js/es/languages/c.js";
import latex from "highlight.js/es/languages/latex";
import csharp_js from "highlight.js/es/languages/csharp.js";
import mel from "highlight.js/es/languages/mel";
import coffeescript from "highlight.js/es/languages/coffeescript";
import gradle_js from "highlight.js/es/languages/gradle.js";
import smalltalk from "highlight.js/es/languages/smalltalk";
import delphi_js from "highlight.js/es/languages/delphi.js";
import leaf_js from "highlight.js/es/languages/leaf.js";
import rust_js from "highlight.js/es/languages/rust.js";
import bnf_js from "highlight.js/es/languages/bnf.js";
import hsp from "highlight.js/es/languages/hsp";
import x86asm from "highlight.js/es/languages/x86asm";
import delphi from "highlight.js/es/languages/delphi";
import ebnf from "highlight.js/es/languages/ebnf";
import php_template from "highlight.js/es/languages/php-template";
import hy_js from "highlight.js/es/languages/hy.js";
import jboss_cli from "highlight.js/es/languages/jboss-cli";
import fortran from "highlight.js/es/languages/fortran";
import dos_js from "highlight.js/es/languages/dos.js";
import apache from "highlight.js/es/languages/apache";
import applescript_js from "highlight.js/es/languages/applescript.js";
import rsl_js from "highlight.js/es/languages/rsl.js";
import smalltalk_js from "highlight.js/es/languages/smalltalk.js";
import ldif from "highlight.js/es/languages/ldif";
import awk_js from "highlight.js/es/languages/awk.js";
import livecodeserver from "highlight.js/es/languages/livecodeserver";
import stata from "highlight.js/es/languages/stata";
import properties from "highlight.js/es/languages/properties";
import ruleslanguage_js from "highlight.js/es/languages/ruleslanguage.js";
import n1ql from "highlight.js/es/languages/n1ql";
import nix_js from "highlight.js/es/languages/nix.js";
import wasm from "highlight.js/es/languages/wasm";
import vala from "highlight.js/es/languages/vala";
import actionscript from "highlight.js/es/languages/actionscript";
import fix_js from "highlight.js/es/languages/fix.js";
import jboss_cli_js from "highlight.js/es/languages/jboss-cli.js";
import matlab_js from "highlight.js/es/languages/matlab.js";
import objectivec_js from "highlight.js/es/languages/objectivec.js";
import nsis from "highlight.js/es/languages/nsis";
import erb from "highlight.js/es/languages/erb";
import css_js from "highlight.js/es/languages/css.js";
import prolog_js from "highlight.js/es/languages/prolog.js";
import dart_js from "highlight.js/es/languages/dart.js";
import gams_js from "highlight.js/es/languages/gams.js";
import scala_js from "highlight.js/es/languages/scala.js";
import latex_js from "highlight.js/es/languages/latex.js";
import erlang_repl_js from "highlight.js/es/languages/erlang-repl.js";
import lasso_js from "highlight.js/es/languages/lasso.js";
import livecodeserver_js from "highlight.js/es/languages/livecodeserver.js";
import sqf_js from "highlight.js/es/languages/sqf.js";
import node_repl from "highlight.js/es/languages/node-repl";
import mercury from "highlight.js/es/languages/mercury";
import xml from "highlight.js/es/languages/xml";
import lsl from "highlight.js/es/languages/lsl";
import actionscript_js from "highlight.js/es/languages/actionscript.js";
import asciidoc_js from "highlight.js/es/languages/asciidoc.js";
import haml_js from "highlight.js/es/languages/haml.js";
import gcode_js from "highlight.js/es/languages/gcode.js";
import ocaml_js from "highlight.js/es/languages/ocaml.js";
import livescript_js from "highlight.js/es/languages/livescript.js";
import verilog_js from "highlight.js/es/languages/verilog.js";
import zephir from "highlight.js/es/languages/zephir";
import moonscript from "highlight.js/es/languages/moonscript";
import bash_js from "highlight.js/es/languages/bash.js";
import plaintext_js from "highlight.js/es/languages/plaintext.js";
import flix_js from "highlight.js/es/languages/flix.js";
import julia_js from "highlight.js/es/languages/julia.js";
import wren_js from "highlight.js/es/languages/wren.js";
import gml from "highlight.js/es/languages/gml";
import nsis_js from "highlight.js/es/languages/nsis.js";
import openscad_js from "highlight.js/es/languages/openscad.js";
import sqf from "highlight.js/es/languages/sqf";
import glsl from "highlight.js/es/languages/glsl";
import hsp_js from "highlight.js/es/languages/hsp.js";
import tp from "highlight.js/es/languages/tp";
import pgsql from "highlight.js/es/languages/pgsql";
import applescript from "highlight.js/es/languages/applescript";
import sml_js from "highlight.js/es/languages/sml.js";
import makefile_js from "highlight.js/es/languages/makefile.js";
import brainfuck from "highlight.js/es/languages/brainfuck";
import yaml_js from "highlight.js/es/languages/yaml.js";
import mizar from "highlight.js/es/languages/mizar";
import mipsasm from "highlight.js/es/languages/mipsasm";
import qml from "highlight.js/es/languages/qml";
import scilab_js from "highlight.js/es/languages/scilab.js";
import ada from "highlight.js/es/languages/ada";
import php from "highlight.js/es/languages/php";
import verilog from "highlight.js/es/languages/verilog";
import vim_js from "highlight.js/es/languages/vim.js";
import erlang_repl from "highlight.js/es/languages/erlang-repl";
import golo_js from "highlight.js/es/languages/golo.js";
import clojure from "highlight.js/es/languages/clojure";
import stylus_js from "highlight.js/es/languages/stylus.js";
import powershell from "highlight.js/es/languages/powershell";
import armasm_js from "highlight.js/es/languages/armasm.js";
import groovy_js from "highlight.js/es/languages/groovy.js";
import llvm_js from "highlight.js/es/languages/llvm.js";
import wasm_js from "highlight.js/es/languages/wasm.js";
import node_repl_js from "highlight.js/es/languages/node-repl.js";
import csp_js from "highlight.js/es/languages/csp.js";
import vbnet from "highlight.js/es/languages/vbnet";
import xl_js from "highlight.js/es/languages/xl.js";
import elixir from "highlight.js/es/languages/elixir";
import llvm from "highlight.js/es/languages/llvm";
import handlebars_js from "highlight.js/es/languages/handlebars.js";
import rib_js from "highlight.js/es/languages/rib.js";
import ruby from "highlight.js/es/languages/ruby";
import nim_js from "highlight.js/es/languages/nim.js";
import csp from "highlight.js/es/languages/csp";
import processing from "highlight.js/es/languages/processing";
import less from "highlight.js/es/languages/less";
import dts from "highlight.js/es/languages/dts";
import lasso from "highlight.js/es/languages/lasso";
import protobuf from "highlight.js/es/languages/protobuf";
import json_js from "highlight.js/es/languages/json.js";
import julia from "highlight.js/es/languages/julia";
import ebnf_js from "highlight.js/es/languages/ebnf.js";
import cmake_js from "highlight.js/es/languages/cmake.js";
import avrasm_js from "highlight.js/es/languages/avrasm.js";
import bnf from "highlight.js/es/languages/bnf";
import clean_js from "highlight.js/es/languages/clean.js";
import nestedtext_js from "highlight.js/es/languages/nestedtext.js";
import groovy from "highlight.js/es/languages/groovy";
import cos from "highlight.js/es/languages/cos";
import nginx_js from "highlight.js/es/languages/nginx.js";
import ceylon_js from "highlight.js/es/languages/ceylon.js";
import coq_js from "highlight.js/es/languages/coq.js";
import scheme from "highlight.js/es/languages/scheme";
import crystal from "highlight.js/es/languages/crystal";
import golo from "highlight.js/es/languages/golo";
import gams from "highlight.js/es/languages/gams";
import isbl from "highlight.js/es/languages/isbl";
import properties_js from "highlight.js/es/languages/properties.js";
import armasm from "highlight.js/es/languages/armasm";
import gauss from "highlight.js/es/languages/gauss";
import kotlin from "highlight.js/es/languages/kotlin";
import monkey from "highlight.js/es/languages/monkey";
import puppet_js from "highlight.js/es/languages/puppet.js";
import django_js from "highlight.js/es/languages/django.js";
import pf_js from "highlight.js/es/languages/pf.js";
import tcl from "highlight.js/es/languages/tcl";
import processing_js from "highlight.js/es/languages/processing.js";
import axapta from "highlight.js/es/languages/axapta";
import dockerfile from "highlight.js/es/languages/dockerfile";
import arduino from "highlight.js/es/languages/arduino";
import ceylon from "highlight.js/es/languages/ceylon";
import inform7_js from "highlight.js/es/languages/inform7.js";
import clojure_js from "highlight.js/es/languages/clojure.js";
import d_js from "highlight.js/es/languages/d.js";
import sas_js from "highlight.js/es/languages/sas.js";
import sml from "highlight.js/es/languages/sml";
import nix from "highlight.js/es/languages/nix";
import irpf90_js from "highlight.js/es/languages/irpf90.js";
import nim from "highlight.js/es/languages/nim";
import go_js from "highlight.js/es/languages/go.js";
import taggerscript_js from "highlight.js/es/languages/taggerscript.js";
import pony from "highlight.js/es/languages/pony";
import reasonml_js from "highlight.js/es/languages/reasonml.js";
import xquery from "highlight.js/es/languages/xquery";
import php_js from "highlight.js/es/languages/php.js";
import tp_js from "highlight.js/es/languages/tp.js";
import plaintext from "highlight.js/es/languages/plaintext";
import clojure_repl_js from "highlight.js/es/languages/clojure-repl.js";
import pgsql_js from "highlight.js/es/languages/pgsql.js";
import gherkin_js from "highlight.js/es/languages/gherkin.js";
import avrasm from "highlight.js/es/languages/avrasm";
import glsl_js from "highlight.js/es/languages/glsl.js";
import thrift from "highlight.js/es/languages/thrift";
import scheme_js from "highlight.js/es/languages/scheme.js";
import objectivec from "highlight.js/es/languages/objectivec";
import typescript from "highlight.js/es/languages/typescript";
import basic_js from "highlight.js/es/languages/basic.js";
import javascript from "highlight.js/es/languages/javascript";
import markdown_js from "highlight.js/es/languages/markdown.js";
import arduino_js from "highlight.js/es/languages/arduino.js";
import roboconf_js from "highlight.js/es/languages/roboconf.js";
import profile_js from "highlight.js/es/languages/profile.js";
import swift from "highlight.js/es/languages/swift";
import ocaml from "highlight.js/es/languages/ocaml";
import _1c_js from "highlight.js/es/languages/1c.js";
import mizar_js from "highlight.js/es/languages/mizar.js";
import makefile from "highlight.js/es/languages/makefile";
import go from "highlight.js/es/languages/go";
import isbl_js from "highlight.js/es/languages/isbl.js";
import nginx from "highlight.js/es/languages/nginx";
import yaml from "highlight.js/es/languages/yaml";
import markdown from "highlight.js/es/languages/markdown";
import vbscript_html_js from "highlight.js/es/languages/vbscript-html.js";
import python_repl_js from "highlight.js/es/languages/python-repl.js";
import http from "highlight.js/es/languages/http";
import scilab from "highlight.js/es/languages/scilab";
import xquery_js from "highlight.js/es/languages/xquery.js";
import capnproto_js from "highlight.js/es/languages/capnproto.js";
import mercury_js from "highlight.js/es/languages/mercury.js";
import python from "highlight.js/es/languages/python";
import q_js from "highlight.js/es/languages/q.js";
import pony_js from "highlight.js/es/languages/pony.js";
import xl from "highlight.js/es/languages/xl";
import subunit from "highlight.js/es/languages/subunit";
import shell from "highlight.js/es/languages/shell";
import thrift_js from "highlight.js/es/languages/thrift.js";
import mojolicious from "highlight.js/es/languages/mojolicious";
import less_js from "highlight.js/es/languages/less.js";
import parser3 from "highlight.js/es/languages/parser3";
import ldif_js from "highlight.js/es/languages/ldif.js";
import parser3_js from "highlight.js/es/languages/parser3.js";
import haskell from "highlight.js/es/languages/haskell";
import flix from "highlight.js/es/languages/flix";
import cmake from "highlight.js/es/languages/cmake";
import dsconfig from "highlight.js/es/languages/dsconfig";
import zephir_js from "highlight.js/es/languages/zephir.js";
import cal from "highlight.js/es/languages/cal";
import twig_js from "highlight.js/es/languages/twig.js";
import twig from "highlight.js/es/languages/twig";
import erlang_js from "highlight.js/es/languages/erlang.js";
import angelscript_js from "highlight.js/es/languages/angelscript.js";
import ada_js from "highlight.js/es/languages/ada.js";
import java_js from "highlight.js/es/languages/java.js";
import dockerfile_js from "highlight.js/es/languages/dockerfile.js";
import capnproto from "highlight.js/es/languages/capnproto";
import autoit_js from "highlight.js/es/languages/autoit.js";
import qml_js from "highlight.js/es/languages/qml.js";
import vbscript_html from "highlight.js/es/languages/vbscript-html";
import protobuf_js from "highlight.js/es/languages/protobuf.js";
import elm_js from "highlight.js/es/languages/elm.js";
import mipsasm_js from "highlight.js/es/languages/mipsasm.js";
import routeros from "highlight.js/es/languages/routeros";
import lisp from "highlight.js/es/languages/lisp";
import vbnet_js from "highlight.js/es/languages/vbnet.js";
import mojolicious_js from "highlight.js/es/languages/mojolicious.js";
import x86asm_js from "highlight.js/es/languages/x86asm.js";
import stata_js from "highlight.js/es/languages/stata.js";
import haml from "highlight.js/es/languages/haml";
import n1ql_js from "highlight.js/es/languages/n1ql.js";
import excel from "highlight.js/es/languages/excel";
import leaf from "highlight.js/es/languages/leaf";
import haxe from "highlight.js/es/languages/haxe";
import crmsh_js from "highlight.js/es/languages/crmsh.js";
import matlab from "highlight.js/es/languages/matlab";
import oxygene from "highlight.js/es/languages/oxygene";
import sql from "highlight.js/es/languages/sql";
import fsharp from "highlight.js/es/languages/fsharp";
import moonscript_js from "highlight.js/es/languages/moonscript.js";
import perl_js from "highlight.js/es/languages/perl.js";
import q from "highlight.js/es/languages/q";
import scala from "highlight.js/es/languages/scala";
import vbscript from "highlight.js/es/languages/vbscript";
import mathematica_js from "highlight.js/es/languages/mathematica.js";
import smali_js from "highlight.js/es/languages/smali.js";
import django from "highlight.js/es/languages/django";
import routeros_js from "highlight.js/es/languages/routeros.js";
import gauss_js from "highlight.js/es/languages/gauss.js";
import arcade from "highlight.js/es/languages/arcade";
import haxe_js from "highlight.js/es/languages/haxe.js";
import abnf_js from "highlight.js/es/languages/abnf.js";
import java from "highlight.js/es/languages/java";
import powershell_js from "highlight.js/es/languages/powershell.js";
import abnf from "highlight.js/es/languages/abnf";
import csharp from "highlight.js/es/languages/csharp";
import step21 from "highlight.js/es/languages/step21";
import haskell_js from "highlight.js/es/languages/haskell.js";
import tap_js from "highlight.js/es/languages/tap.js";
import brainfuck_js from "highlight.js/es/languages/brainfuck.js";
import javascript_js from "highlight.js/es/languages/javascript.js";
import apache_js from "highlight.js/es/languages/apache.js";
import sql_js from "highlight.js/es/languages/sql.js";
import xml_js from "highlight.js/es/languages/xml.js";
import elixir_js from "highlight.js/es/languages/elixir.js";
import dsconfig_js from "highlight.js/es/languages/dsconfig.js";
import subunit_js from "highlight.js/es/languages/subunit.js";
import ruleslanguage from "highlight.js/es/languages/ruleslanguage";
import dns_js from "highlight.js/es/languages/dns.js";
import http_js from "highlight.js/es/languages/http.js";
import aspectj from "highlight.js/es/languages/aspectj";
import dust_js from "highlight.js/es/languages/dust.js";
import diff from "highlight.js/es/languages/diff";
import shell_js from "highlight.js/es/languages/shell.js";
import cpp from "highlight.js/es/languages/cpp";
import excel_js from "highlight.js/es/languages/excel.js";
import ruby_js from "highlight.js/es/languages/ruby.js";
import c from "highlight.js/es/languages/c";
import erb_js from "highlight.js/es/languages/erb.js";
import arcade_js from "highlight.js/es/languages/arcade.js";
import lua from "highlight.js/es/languages/lua";
import _1c from "highlight.js/es/languages/1c";
import vala_js from "highlight.js/es/languages/vala.js";
import dns from "highlight.js/es/languages/dns";
import lsl_js from "highlight.js/es/languages/lsl.js";
import json from "highlight.js/es/languages/json";
import accesslog from "highlight.js/es/languages/accesslog";
import dart from "highlight.js/es/languages/dart";
import erlang from "highlight.js/es/languages/erlang";
import accesslog_js from "highlight.js/es/languages/accesslog.js";
import stylus from "highlight.js/es/languages/stylus";
import rib from "highlight.js/es/languages/rib";
import aspectj_js from "highlight.js/es/languages/aspectj.js";
import stan_js from "highlight.js/es/languages/stan.js";
import vhdl from "highlight.js/es/languages/vhdl";
import crystal_js from "highlight.js/es/languages/crystal.js";
import taggerscript from "highlight.js/es/languages/taggerscript";
import tap from "highlight.js/es/languages/tap";
import julia_repl from "highlight.js/es/languages/julia-repl";
import hy from "highlight.js/es/languages/hy";
import cal_js from "highlight.js/es/languages/cal.js";
import kotlin_js from "highlight.js/es/languages/kotlin.js";
import profile from "highlight.js/es/languages/profile";
import r_js from "highlight.js/es/languages/r.js";
import dust from "highlight.js/es/languages/dust";

export default {
  "cpp.js": cpp_js,
  gradle: gradle,
  "lisp.js": lisp_js,
  maxima: maxima,
  "python-repl": python_repl,
  "dts.js": dts_js,
  fix: fix,
  "monkey.js": monkey_js,
  "oxygene.js": oxygene_js,
  bash: bash,
  "purebasic.js": purebasic_js,
  "cos.js": cos_js,
  rust: rust,
  clean: clean,
  angelscript: angelscript,
  "lua.js": lua_js,
  inform7: inform7,
  livescript: livescript,
  "autohotkey.js": autohotkey_js,
  "axapta.js": axapta_js,
  puppet: puppet,
  "swift.js": swift_js,
  pf: pf,
  smali: smali,
  "step21.js": step21_js,
  "tcl.js": tcl_js,
  "julia-repl.js": julia_repl_js,
  "php-template.js": php_template_js,
  "python.js": python_js,
  coq: coq,
  dos: dos,
  "vbscript.js": vbscript_js,
  "ini.js": ini_js,
  perl: perl,
  stan: stan,
  irpf90: irpf90,
  ini: ini,
  nestedtext: nestedtext,
  "coffeescript.js": coffeescript_js,
  vim: vim,
  wren: wren,
  crmsh: crmsh,
  prolog: prolog,
  "scss.js": scss_js,
  "fortran.js": fortran_js,
  "clojure-repl": clojure_repl,
  awk: awk,
  elm: elm,
  asciidoc: asciidoc,
  css: css,
  gherkin: gherkin,
  autoit: autoit,
  d: d,
  rsl: rsl,
  autohotkey: autohotkey,
  "diff.js": diff_js,
  sas: sas,
  openscad: openscad,
  "mel.js": mel_js,
  gcode: gcode,
  handlebars: handlebars,
  "fsharp.js": fsharp_js,
  roboconf: roboconf,
  scss: scss,
  mathematica: mathematica,
  reasonml: reasonml,
  r: r,
  purebasic: purebasic,
  "typescript.js": typescript_js,
  "maxima.js": maxima_js,
  basic: basic,
  "vhdl.js": vhdl_js,
  "gml.js": gml_js,
  "c.js": c_js,
  latex: latex,
  "csharp.js": csharp_js,
  mel: mel,
  coffeescript: coffeescript,
  "gradle.js": gradle_js,
  smalltalk: smalltalk,
  "delphi.js": delphi_js,
  "leaf.js": leaf_js,
  "rust.js": rust_js,
  "bnf.js": bnf_js,
  hsp: hsp,
  x86asm: x86asm,
  delphi: delphi,
  ebnf: ebnf,
  "php-template": php_template,
  "hy.js": hy_js,
  "jboss-cli": jboss_cli,
  fortran: fortran,
  "dos.js": dos_js,
  apache: apache,
  "applescript.js": applescript_js,
  "rsl.js": rsl_js,
  "smalltalk.js": smalltalk_js,
  ldif: ldif,
  "awk.js": awk_js,
  livecodeserver: livecodeserver,
  stata: stata,
  properties: properties,
  "ruleslanguage.js": ruleslanguage_js,
  n1ql: n1ql,
  "nix.js": nix_js,
  wasm: wasm,
  vala: vala,
  actionscript: actionscript,
  "fix.js": fix_js,
  "jboss-cli.js": jboss_cli_js,
  "matlab.js": matlab_js,
  "objectivec.js": objectivec_js,
  nsis: nsis,
  erb: erb,
  "css.js": css_js,
  "prolog.js": prolog_js,
  "dart.js": dart_js,
  "gams.js": gams_js,
  "scala.js": scala_js,
  "latex.js": latex_js,
  "erlang-repl.js": erlang_repl_js,
  "lasso.js": lasso_js,
  "livecodeserver.js": livecodeserver_js,
  "sqf.js": sqf_js,
  "node-repl": node_repl,
  mercury: mercury,
  xml: xml,
  lsl: lsl,
  "actionscript.js": actionscript_js,
  "asciidoc.js": asciidoc_js,
  "haml.js": haml_js,
  "gcode.js": gcode_js,
  "ocaml.js": ocaml_js,
  "livescript.js": livescript_js,
  "verilog.js": verilog_js,
  zephir: zephir,
  moonscript: moonscript,
  "bash.js": bash_js,
  "plaintext.js": plaintext_js,
  "flix.js": flix_js,
  "julia.js": julia_js,
  "wren.js": wren_js,
  gml: gml,
  "nsis.js": nsis_js,
  "openscad.js": openscad_js,
  sqf: sqf,
  glsl: glsl,
  "hsp.js": hsp_js,
  tp: tp,
  pgsql: pgsql,
  applescript: applescript,
  "sml.js": sml_js,
  "makefile.js": makefile_js,
  brainfuck: brainfuck,
  "yaml.js": yaml_js,
  mizar: mizar,
  mipsasm: mipsasm,
  qml: qml,
  "scilab.js": scilab_js,
  ada: ada,
  php: php,
  verilog: verilog,
  "vim.js": vim_js,
  "erlang-repl": erlang_repl,
  "golo.js": golo_js,
  clojure: clojure,
  "stylus.js": stylus_js,
  powershell: powershell,
  "armasm.js": armasm_js,
  "groovy.js": groovy_js,
  "llvm.js": llvm_js,
  "wasm.js": wasm_js,
  "node-repl.js": node_repl_js,
  "csp.js": csp_js,
  vbnet: vbnet,
  "xl.js": xl_js,
  elixir: elixir,
  llvm: llvm,
  "handlebars.js": handlebars_js,
  "rib.js": rib_js,
  ruby: ruby,
  "nim.js": nim_js,
  csp: csp,
  processing: processing,
  less: less,
  dts: dts,
  lasso: lasso,
  protobuf: protobuf,
  "json.js": json_js,
  julia: julia,
  "ebnf.js": ebnf_js,
  "cmake.js": cmake_js,
  "avrasm.js": avrasm_js,
  bnf: bnf,
  "clean.js": clean_js,
  "nestedtext.js": nestedtext_js,
  groovy: groovy,
  cos: cos,
  "nginx.js": nginx_js,
  "ceylon.js": ceylon_js,
  "coq.js": coq_js,
  scheme: scheme,
  crystal: crystal,
  golo: golo,
  gams: gams,
  isbl: isbl,
  "properties.js": properties_js,
  armasm: armasm,
  gauss: gauss,
  kotlin: kotlin,
  monkey: monkey,
  "puppet.js": puppet_js,
  "django.js": django_js,
  "pf.js": pf_js,
  tcl: tcl,
  "processing.js": processing_js,
  axapta: axapta,
  dockerfile: dockerfile,
  arduino: arduino,
  ceylon: ceylon,
  "inform7.js": inform7_js,
  "clojure.js": clojure_js,
  "d.js": d_js,
  "sas.js": sas_js,
  sml: sml,
  nix: nix,
  "irpf90.js": irpf90_js,
  nim: nim,
  "go.js": go_js,
  "taggerscript.js": taggerscript_js,
  pony: pony,
  "reasonml.js": reasonml_js,
  xquery: xquery,
  "php.js": php_js,
  "tp.js": tp_js,
  plaintext: plaintext,
  "clojure-repl.js": clojure_repl_js,
  "pgsql.js": pgsql_js,
  "gherkin.js": gherkin_js,
  avrasm: avrasm,
  "glsl.js": glsl_js,
  thrift: thrift,
  "scheme.js": scheme_js,
  objectivec: objectivec,
  typescript: typescript,
  "basic.js": basic_js,
  javascript: javascript,
  "markdown.js": markdown_js,
  "arduino.js": arduino_js,
  "roboconf.js": roboconf_js,
  "profile.js": profile_js,
  swift: swift,
  ocaml: ocaml,
  "1c.js": _1c_js,
  "mizar.js": mizar_js,
  makefile: makefile,
  go: go,
  "isbl.js": isbl_js,
  nginx: nginx,
  yaml: yaml,
  markdown: markdown,
  "vbscript-html.js": vbscript_html_js,
  "python-repl.js": python_repl_js,
  http: http,
  scilab: scilab,
  "xquery.js": xquery_js,
  "capnproto.js": capnproto_js,
  "mercury.js": mercury_js,
  python: python,
  "q.js": q_js,
  "pony.js": pony_js,
  xl: xl,
  subunit: subunit,
  shell: shell,
  "thrift.js": thrift_js,
  mojolicious: mojolicious,
  "less.js": less_js,
  parser3: parser3,
  "ldif.js": ldif_js,
  "parser3.js": parser3_js,
  haskell: haskell,
  flix: flix,
  cmake: cmake,
  dsconfig: dsconfig,
  "zephir.js": zephir_js,
  cal: cal,
  "twig.js": twig_js,
  twig: twig,
  "erlang.js": erlang_js,
  "angelscript.js": angelscript_js,
  "ada.js": ada_js,
  "java.js": java_js,
  "dockerfile.js": dockerfile_js,
  capnproto: capnproto,
  "autoit.js": autoit_js,
  "qml.js": qml_js,
  "vbscript-html": vbscript_html,
  "protobuf.js": protobuf_js,
  "elm.js": elm_js,
  "mipsasm.js": mipsasm_js,
  routeros: routeros,
  lisp: lisp,
  "vbnet.js": vbnet_js,
  "mojolicious.js": mojolicious_js,
  "x86asm.js": x86asm_js,
  "stata.js": stata_js,
  haml: haml,
  "n1ql.js": n1ql_js,
  excel: excel,
  leaf: leaf,
  haxe: haxe,
  "crmsh.js": crmsh_js,
  matlab: matlab,
  oxygene: oxygene,
  sql: sql,
  fsharp: fsharp,
  "moonscript.js": moonscript_js,
  "perl.js": perl_js,
  q: q,
  scala: scala,
  vbscript: vbscript,
  "mathematica.js": mathematica_js,
  "smali.js": smali_js,
  django: django,
  "routeros.js": routeros_js,
  "gauss.js": gauss_js,
  arcade: arcade,
  "haxe.js": haxe_js,
  "abnf.js": abnf_js,
  java: java,
  "powershell.js": powershell_js,
  abnf: abnf,
  csharp: csharp,
  step21: step21,
  "haskell.js": haskell_js,
  "tap.js": tap_js,
  "brainfuck.js": brainfuck_js,
  "javascript.js": javascript_js,
  "apache.js": apache_js,
  "sql.js": sql_js,
  "xml.js": xml_js,
  "elixir.js": elixir_js,
  "dsconfig.js": dsconfig_js,
  "subunit.js": subunit_js,
  ruleslanguage: ruleslanguage,
  "dns.js": dns_js,
  "http.js": http_js,
  aspectj: aspectj,
  "dust.js": dust_js,
  diff: diff,
  "shell.js": shell_js,
  cpp: cpp,
  "excel.js": excel_js,
  "ruby.js": ruby_js,
  c: c,
  "erb.js": erb_js,
  "arcade.js": arcade_js,
  lua: lua,
  "1c": _1c,
  "vala.js": vala_js,
  dns: dns,
  "lsl.js": lsl_js,
  json: json,
  accesslog: accesslog,
  dart: dart,
  erlang: erlang,
  "accesslog.js": accesslog_js,
  stylus: stylus,
  rib: rib,
  "aspectj.js": aspectj_js,
  "stan.js": stan_js,
  vhdl: vhdl,
  "crystal.js": crystal_js,
  taggerscript: taggerscript,
  tap: tap,
  "julia-repl": julia_repl,
  hy: hy,
  "cal.js": cal_js,
  "kotlin.js": kotlin_js,
  profile: profile,
  "r.js": r_js,
  dust: dust,
};
